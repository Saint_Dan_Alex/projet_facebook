import 'package:flutter/material.dart';
import 'package:projet_groupe2/modele/NotificationModele.dart';
import 'package:projet_groupe2/pages/IntroPage.dart';
import 'package:projet_groupe2/pages/LoginPage.dart';
import 'package:projet_groupe2/pages/NotificationPage.dart';
import 'package:projet_groupe2/pages/carousel.dart';

import '../pages/test.dart';

class MonApplication extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false ,
      home: CarouselPage(),
    );
  }
}