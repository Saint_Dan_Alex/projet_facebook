import 'package:flutter/material.dart';
import 'package:projet_groupe2/pages/LoginPage.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();

}

class _IntroPageState extends State<IntroPage> {
  @override
  Color couleurFond = Colors.white;
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: couleurFond,
      body:
      _body(),);
  }
  Widget _body(){
    return Container(
      width: double.infinity,
      padding:  EdgeInsets.symmetric(horizontal: 20),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              _iconApp(),
              SizedBox(
                height: 2,
              ),

              _iconMoreHoriz(),
              SizedBox(
                height: 30,
              ),

            ],),
        ),
      ),
    );
  }

  Widget _iconApp(){
    return Icon(
      Icons.facebook,
      size: 100,
      color: Colors.blue,);
  }
  Widget _iconMoreHoriz(){
    return Icon(
      Icons.more_horiz,
      size: 100,
      color: Colors.grey,);
  }

}
