import 'package:flutter/material.dart';
import 'package:projet_groupe2/modele/NotificationModele.dart';
import 'package:projet_groupe2/utils/Constantes.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar(){
    return AppBar(title: Text("Notification (0)")
      ,backgroundColor: Colors.blue,elevation: 0,actions: [
        Icon(Icons.add_alert)
      ],);
  }
  Widget _body(){
    return ListView.builder
    (
        itemCount: Constantes.notification.length,
    itemBuilder: (ctx,i){
      var notification = Constantes.notification[i];
      var f= NotificationModele.fromJson(notification);
      return ListTile(
        title: Text("${f.titre}"),
        subtitle: Text("${f.temps}"),
        trailing: Icon(Icons.more_horiz),
        leading: f.image != null ? Image.asset(f.image!): Icon(Icons.error),
      );


     });
  }
}



