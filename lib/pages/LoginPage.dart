import 'package:flutter/material.dart';
import 'package:projet_groupe2/pages/NotificationPage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Color couleurFond = Colors.white;
  String errorMsg = "";
  @override
  Widget build(BuildContext context) {
      return Scaffold(backgroundColor: couleurFond,
        body:
        _body(),);
    }

  Widget _body(){
    return Container(
      width: double.infinity,
      padding:  EdgeInsets.symmetric(horizontal: 20),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _forget(),
              SizedBox(
                height: 40,
              ),
              _iconApp(),
              SizedBox(
                height: 30,
              ),
              Text("Sign in"),
              SizedBox(
                height: 40,
              ),
              _loginSaisie(),
              SizedBox(
                height: 20,
              ),
              _loginSaisie(),
              SizedBox(
                height: 20,
              ),
              _buttonWidget(),
              SizedBox(
                height: 10,
              ),
              _textError(),
              SizedBox(
                height: 30,
              ),
              _singUp(),
              SizedBox(
                height: 30,
              ),
              _button(),
            ],),
        ),
      ),
    );
  }

  Widget _iconApp(){
    return Icon(
      Icons.facebook,
      size: 100,
      color: Colors.blue,);
  }

  Widget _loginSaisie(){
    return TextField(
      decoration: InputDecoration(
        border: _bordure(Colors.grey),
        focusedBorder: _bordure(Colors.orange),
        enabledBorder: _bordure(Colors.grey) ,
        labelText: 'Entrez le nom ici',
        hintText: 'Entrez le nom ici',
      ),
      autofocus: false,
    );
    // creer une zone de saisie
  }

  Widget _buttonWidget(){
    return Container(
      width: 500,
      height: 50,
      child: ElevatedButton(onPressed: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>NotificationPage()),
        );

        setState(() {

        });
        errorMsg = "Mot de passe incorect";
        //couleurFond=Colors.black;
        if(couleurFond==Colors.blue){
          couleurFond=Colors.white;
        }else{
          couleurFond=Colors.white;
          errorMsg = "";
        }
        setState(() {
          {}
        });
      },
        child: Text("Se connecter"), style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        ),),
    );
  }

  OutlineInputBorder _bordure(MaterialColor _color){
    return OutlineInputBorder(
        borderSide: BorderSide(width: 2,
            color: _color),
        borderRadius: BorderRadius.all(Radius.circular(16)));
  }
  Widget _textError(){
    return Text(errorMsg, style: TextStyle(color: Colors.red,fontSize: 16),);
  }
  Widget _singUp() {
    return Text("Mot de passe oublié ?");
  }

  Widget _forget() {
    return Text("Fracçais (France)");
  }

  Widget _button(){
    return Container(
        child: Text("Créer un compte", style: TextStyle(color: Colors.grey,fontSize: 16)),
    );
  }
}


