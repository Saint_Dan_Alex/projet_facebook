import 'package:flutter/material.dart';

import 'LoginPage.dart';

class CarouselPage extends StatefulWidget {
  const CarouselPage({Key? key}) : super(key: key);

  @override
  State<CarouselPage> createState() => _CarouselPageState();
}

class _CarouselPageState extends State<CarouselPage> {
  @override
  Color couleurFond = Colors.white;
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      _body(),
    );
  }
  Widget _body(){
    return Container(
      width: double.infinity,
      padding:  EdgeInsets.symmetric(horizontal: 20),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _iconApp(),
              _hope(),
              SizedBox(
                height: 300,
              ),
              _fromTxt(),
              Container(
                  width: 100,
                  child:
              Image.asset("assets/logo1.png",)),

            ],),
        ),
      ),
    );
  }
  Widget _iconApp(){
    return Icon(
      Icons.facebook,
      size: 150,
      color: Colors.blue,);
  }

  Widget _hope() {
    return Text(".....", style: TextStyle(fontSize: 50),);
  }

  Widget _fromTxt() {
    return Text("From", style: TextStyle(fontSize: 20));
  }

}
