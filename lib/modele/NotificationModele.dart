// To parse this JSON data, do
//
//     final notificationModele = notificationModeleFromJson(jsonString);

import 'dart:convert';

NotificationModele notificationModeleFromJson(String str) => NotificationModele.fromJson(json.decode(str));

String notificationModeleToJson(NotificationModele data) => json.encode(data.toJson());

class NotificationModele {
  NotificationModele({
    this.id,
    this.titre,
    this.temps,
    this.image,
  });

  int? id;
  String? titre;
  String? temps;
  String? image;

  factory NotificationModele.fromJson(Map json) => NotificationModele(
    id: json["id"],
    titre: json["titre"],
    temps: json["temps"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "titre": titre,
    "temps": temps,
    "image": image,
  };
}
